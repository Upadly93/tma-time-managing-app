// Angular and Ionic modules
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ToastController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';

// Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PomodoroPage } from '../pages/pomodoro/pomodoro';
import { RegistrationPage } from '../pages/registration/registration';
import { TimeMarkerPage } from '../pages/timeMarker/timeMarker';
import { HistoryPage } from '../pages/history/history';
import { TagsAddPage } from '../pages/tagsAdd/tagsAdd';
import { TagsListPage } from '../pages/tagsList/tagsList';
import { SettingsPage } from '../pages/settings/settings';
import { Calendar } from '@ionic-native/calendar';
import { AddEventPage } from '../pages/addEvent/addEvent';
import { EditEventPage } from '../pages/editEvent/editEvent';
import { ResetPasswordPage } from '../pages/resetPassword/resetPassword';
import { PomodoroTimer } from '../providers/PomodoroTimer';

// Firebase
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// Storage
import { IonicStorageModule } from '@ionic/storage';

// helpers
import { TranslationHelper } from '../helpers/translation';
import { DateHelper } from '../helpers/date';

const firebaseConfig = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAwin3c2gFKvw6tKALCnS-U0v5M-pDhibQ",
    authDomain: "tmagl-2762c.firebaseapp.com",
    databaseURL: "https://tmagl-2762c.firebaseio.com",
    projectId: "tmagl-2762c",
    storageBucket: "tmagl-2762c.appspot.com",
    messagingSenderId: "1019035331320"
  }
};

export function createTranslateLoader(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    PomodoroPage,
    RegistrationPage,
    TimeMarkerPage,
    HistoryPage,
    TagsAddPage,
    TagsListPage,
    SettingsPage,
    AddEventPage,
    EditEventPage,
    ResetPasswordPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig.firebase),
    AngularFireDatabaseModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [
    IonicApp,
  ],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    PomodoroPage,
    RegistrationPage,
    TimeMarkerPage,
    HistoryPage,
    TagsAddPage,
    TagsListPage,
    SettingsPage,
    AddEventPage,
    EditEventPage,
    ResetPasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireAuth,
    Calendar,
    ToastController,
    PomodoroTimer,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    TranslationHelper,
    DatePicker,
    DateHelper,
  ]
})

export class AppModule { }