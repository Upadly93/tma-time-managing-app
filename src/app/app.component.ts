import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PomodoroPage } from '../pages/pomodoro/pomodoro';
import { TagsListPage } from '../pages/tagsList/tagsList';
import { HistoryPage } from '../pages/history/history';
import { SettingsPage } from '../pages/settings/settings';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  pages: Array<{ title: String, component: any, icon: String }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private storage: Storage, public translate: TranslateService) {
    this.initialApp();
    this.initSettings();

    this.storage.set('isLogged', false);
  }

  /**
   * Method to initial all application settings
   */
  initSettings() {
    this.initPomodoro();
    this.initTranslate();
    this.setHamburgerPages();
  }

  /**
   * Method to initial application language
   */
  initTranslate() {
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    this.storage.get('appLanguage').then(val => {
      if (val !== null && val !== undefined) {
        this.translate.use(val);
      } else if (browserLang) {
        this.translate.use(browserLang);
        this.storage.set('appLanguage', browserLang);
      } else {
        this.translate.use('en');
        this.storage.set('appLanguage', 'en');
      }
    })
  }

  /**
   * Method to initial pomodoro settings
   */
  initPomodoro() {
    const pomodoro = {
      time: 30,
      loop: 5
    }

    this.storage.set('pomodoro', pomodoro);
  }

  setHamburgerPages() {
    this.pages = [
      { title: "MenuBar.Podgląd", component: HomePage, icon: "eye" },
      { title: "MenuBar.Pomodoro", component: PomodoroPage, icon: "timer" },
      { title: "MenuBar.Tagi", component: TagsListPage, icon: "pricetags" },
      { title: "MenuBar.Historia", component: HistoryPage, icon: "clock" },
      { title: "MenuBar.Ustawienia", component: SettingsPage, icon: "person" },
      { title: "MenuBar.Wyloguj", component: "", icon: "log-out" },
    ];
  }

  /**
   * Method to initial application
   */
  initialApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * Method to logout
   * @param page 
   */
  openPage(page) {
    if (page.title === "MenuBar.Wyloguj") {
      this.storage.set('isLogged', false);
      this.storage.remove('email');
      this.nav.setRoot(LoginPage);
    } else {
      this.nav.setRoot(page.component);
    }
  }
}

