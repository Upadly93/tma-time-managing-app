import { Component } from '@angular/core';
import { NavController, AlertController, MenuController } from 'ionic-angular';
import { AddEventPage } from '../addEvent/addEvent';
import { Calendar } from '@ionic-native/calendar';
import { TranslateService } from '@ngx-translate/core';
import { EditEventPage } from '../editEvent/editEvent';
import { TranslationHelper } from '../../helpers/translation';
import { DateHelper } from '../../helpers/date';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  eventList: Array<any>;
  selectedEvent: Array<any>;
  isSelected: boolean;
  date: Date;
  daysInThisMonth: Array<number>;
  daysInLastMonth: Array<number>;
  daysInNextMonth: Array<number>;
  monthNames: Array<string>;
  currentMonth: string;
  currentYear: number;
  selectedDay: number;
  currentDate: number;

  constructor(private alertCtrl: AlertController,
    public navCtrl: NavController,
    private calendar: Calendar,
    public menuCtrl: MenuController,
    public translate: TranslateService,
    private translationHelper: TranslationHelper,
    private dateHelper: DateHelper) {
    this.menuCtrl.enable(true, 'mainMenu');
    this.date = new Date();
    this.setMonthName();
    this.selectedDay = this.date.getDate();
  }

  /**
   * Method run when user enter to this page
   */
  ionViewWillEnter() {
    this.getDaysOfMonth();
    this.loadEventThisMonth();
  }

  /**
   * Methot to set all month name
   */
  setMonthName() {
    this.monthNames = [
      this.translationHelper.getTranslation("Calendar", "Styczen"), this.translationHelper.getTranslation("Calendar", "Luty"), this.translationHelper.getTranslation("Calendar", "Marzec"),
      this.translationHelper.getTranslation("Calendar", "Kwiecien"), this.translationHelper.getTranslation("Calendar", "Maj"), this.translationHelper.getTranslation("Calendar", "Czerwiec"),
      this.translationHelper.getTranslation("Calendar", "Lipiec"), this.translationHelper.getTranslation("Calendar", "Sierpien"), this.translationHelper.getTranslation("Calendar", "Wrzesien"),
      this.translationHelper.getTranslation("Calendar", "Pazdziernik"), this.translationHelper.getTranslation("Calendar", "Listopad"), this.translationHelper.getTranslation("Calendar", "Grudzien")
    ];
  }

  /**
   * Method to get all day of month
   */
  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    const firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    const prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (let i = prevNumOfDays - (firstDayThisMonth + 5); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    const thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (let j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j + 1);
    }

    const lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    for (let k = 0; k < (7 - lastDayThisMonth); k++) {
      this.daysInNextMonth.push(k + 1);
    }
    const totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (let l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
        this.daysInNextMonth.push(l);
      }
    }
  }

  /**
   * Method to set a previous month
   */
  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  /**
   * Method to set a next month
   */
  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
  }

  /**
   * Method to redirect to AddEventPage
   */
  addEvent() {
    this.navCtrl.push(AddEventPage, {
      selectedDate: new Date(this.date.getFullYear(), this.date.getMonth() + 1, this.selectedDay).getTime()
    });
  }

  /**
   * Method to redirect to edit selected event
   * @param evn selected event to edit
   */
  editEvent(evn) {
    this.navCtrl.setRoot(EditEventPage, {
      event: evn
    });
  }

  /**
   * Method to get all events of this month
   */
  loadEventThisMonth() {
    this.eventList = new Array();
    const startDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    const endDate = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0);

    this.calendar.listEventsInRange(startDate, endDate).then(
      (msg) => {
        msg.forEach(item => {
          item.formatedDateStart = this.dateHelper.dateFormater(item.dtstart);
          item.formatedDateEnd = this.dateHelper.dateFormater(item.dtend);
          this.eventList.push(item);
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }

  /**
   * Method to check if some day of month has as event
   * @param day number Day of month
   */
  checkEvent(day) {
    let hasEvent = false;
    const dateStart = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 00:00:00").getTime();
    const dateEnd = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 23:59:59").getTime();

    this.eventList.forEach(event => {
      if (((event.dtstart >= dateStart) && (event.dtstart <= dateEnd)) || ((event.dtend >= dateStart) && (event.dtend <= dateEnd))) {
        hasEvent = true;
      }
    });
    return hasEvent;
  }

  /**
   * Method to set events for selected day
   * @param day number Selected day by user
   */
  selectDate(day) {
    this.isSelected = false;
    this.selectedEvent = new Array();
    const dateStart = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 00:00:00").getTime();
    const dateEnd = new Date(this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + day + " 23:59:59").getTime();

    this.eventList.forEach(event => {
      if (((event.dtstart >= dateStart) && (event.dtstart < dateEnd)) || ((event.dtend >= dateStart) && (event.dtend < dateEnd))) {
        this.isSelected = true;
        this.selectedEvent.push(event);
      }
    });

    this.selectedDay = day;
  }

  /**
   * Method to deleted event from calendar
   * @param evt Event Object event from calendar
   */
  deleteEvent(evt) {
    let alert = this.alertCtrl.create({
      title: this.translationHelper.getTranslation("Toast", "Potwierdź usunięcie"),
      message: this.translationHelper.getTranslation("Toast", "Jesteś pewien czy chcesz usunąć te zdarzenie?"),
      buttons: [
        {
          text: this.translationHelper.getTranslation("Toast", "Anuluj"),
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.calendar.deleteEvent(evt.title, evt.location, evt.notes, new Date(evt.dtstart), new Date(evt.dtstart)).then(
              (msg) => {
                this.loadEventThisMonth();
                this.selectDate(this.selectedDay);
              },
              (err) => {
                console.log(err);
              }
            )
          }
        }
      ]
    });
    alert.present();
  }
}
