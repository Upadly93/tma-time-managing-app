import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { RegistrationPage } from '../registration/registration';
import { HomePage } from '../home/home';
import { ResetPasswordPage } from '../resetPassword/resetPassword';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TranslationHelper } from '../../helpers/translation';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage implements OnInit {
    model = {
        "email": "",
        "password": ""
    };
    loginForm: FormGroup;
    validationMessages: Object;

    constructor(public navCtrl: NavController,
        public menuCtrl: MenuController,
        public afa: AngularFireAuth,
        private storage: Storage,
        public translate: TranslateService,
        private translationHelper: TranslationHelper) {
        this.menuCtrl.enable(false, 'mainMenu');
        this.setValidMessage();
    }

    /**
     * Method to setting the validation message
     */
    setValidMessage(){
        this.validationMessages = {
            'email': [
                { type: 'required', message: 'Proszę podaj adres e-mail' },
                { type: 'pattern', message: 'Adres e-mail jest nieprawidłowy' },
            ],
            'password': [
                { type: 'required', message: 'Proszę podaj hasło' },
                { type: 'minlength', message: 'Hasło jest za krótkie (min 6 znaków)' },
                { type: 'maxlength', message: 'Hasło jest za długie (max 32 znaków)' },
            ],
        }
    }

    /**
     * Method to log in user
     */
    async logIn() {

        /* If form is not valid */
        if (this.loginForm.valid === false) {
            return;
        }

        let errorContainer = document.getElementById("errorContainer");
        let errorTextContainer = document.getElementById("errorTextContainer");

        // default not visible
        errorContainer.style.display = "none";

        const loginUser = (navCtrl, storage, model, translationHelper) => {
            this.afa.auth.signInWithEmailAndPassword(this.model.email, this.model.password).then(function (user) {
                storage.set('isLogged', true);
                storage.set('email', model.email);
                navCtrl.setRoot(HomePage);
            }, function (error) {

                /**
                 * Function set error message in a form header
                 * @param message String
                 */
                function setErrorMessage(message: String) {
                    errorContainer.style.display = "block";
                    errorTextContainer.innerText = translationHelper.getTranslation("AuthValidation", message);
                }

                switch (error.code) {
                    case 'auth/invalid-email':
                        setErrorMessage("Niepoprawny adres e-mail");
                        break;
                    case 'auth/user-disabled':
                        setErrorMessage("Użytkownik ma zablokowane konto");
                        break;
                    case 'auth/user-not-found':
                        setErrorMessage("Użytkownik nie został znaleziony");
                        break;
                    case 'auth/wrong-password':
                        setErrorMessage("Dane logowania są nieprawidłowe");
                        break;
                    default:
                        setErrorMessage(error.message);
                }
            });
        }

        loginUser.call(this, this.navCtrl, this.storage, this.model, this.translationHelper);
    }

    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';

    /**
     * Method for refreshing eye icon to hide and show password in form
     */
    hideShowPassword() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }

    /**
     * Init login form
     */
    ngOnInit() {
        let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
            password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]),
        });
    }

    /**
     * Method for forgot pass button
     */
    forgotPass() {
        this.navCtrl.setRoot(ResetPasswordPage);
    }

    /**
     * Method for create account button
     */
    createAcc() {
        this.navCtrl.setRoot(RegistrationPage);
    }

}