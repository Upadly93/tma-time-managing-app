import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { TranslateService } from '@ngx-translate/core';
import { DatePicker } from '@ionic-native/date-picker';
import { TranslationHelper } from '../../helpers/translation';
import { DateHelper } from '../../helpers/date';

@IonicPage()
@Component({
  selector: 'page-add-event',
  templateUrl: 'addEvent.html',
})
export class AddEventPage {
  event = { title: "", eventLocation: "", notes: "", startDate: "", endDate: "" };
  selectedDate: number;
  timestampStart: number;
  timestampEnd: number;

  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams,
    private calendar: Calendar,
    public translate: TranslateService,
    public datePicker: DatePicker,
    private translationHelper: TranslationHelper,
    private dateHelper: DateHelper) {
    this.menuCtrl.enable(true, 'mainMenu');
    this.selectedDate = navParams.get("selectedDate");
  }
  
  /**
   * Method run when user enter to this page
   */
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventPage');
  }

  /**
   * Method to save a new event
   */
  save() {
    this.calendar.createEvent(this.event.title, this.event.eventLocation, this.event.notes, new Date(this.timestampStart), new Date(this.timestampEnd)).then(
      (msg) => {
        let alert = this.alertCtrl.create({
          title: this.translationHelper.getTranslation("Toast", "Potwierdzenie"),
          subTitle: this.translationHelper.getTranslation("Toast", "Zdarzenie dodano poprawnie"),
          buttons: ['OK']
        });
        alert.present();
        this.navCtrl.pop();
      },
      (err) => {
        let alert = this.alertCtrl.create({
          title: this.translationHelper.getTranslation("Toast", "Błąd!"),
          subTitle: err,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }

  /**
   * Method to display native-datapicker to set a start data event
   */
  showDateStart() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      is24Hour: true
    }).then(
      date => {
        this.event.startDate = this.dateHelper.dateFormater(date);
        this.timestampStart = new Date(date).getTime()
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  /**
   * Method to display native-datapicker to set a end data event
   */
  showDateEnd() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      is24Hour: true
    }).then(
      date => {
        this.event.endDate = this.dateHelper.dateFormater(date);
        this.timestampEnd = new Date(date).getTime()
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }
}