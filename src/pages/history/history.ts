import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Calendar } from '@ionic-native/calendar';
import { DateHelper } from '../../helpers/date';

@Component({
    selector: 'page-history',
    templateUrl: 'history.html'
})
export class HistoryPage {
    eventList: Array<Object>;
    date: Date;

    constructor(public navCtrl: NavController,
        public menuCtrl: MenuController,
        public translate: TranslateService,
        private calendar: Calendar,
        private dateHelper: DateHelper) {
        this.menuCtrl.enable(true, 'mainMenu');
        this.date = new Date();
    }

    /**
     * Method run when user enter to this page
     */
    ionViewWillEnter() {
        this.loadEventsForHistory();
    }

    /**
     * Method to load events for history
     */
    loadEventsForHistory() {
        this.eventList = new Array();
        const startDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        const endDate = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());

        this.calendar.listEventsInRange(startDate, endDate).then(
            (msg) => {
                msg.forEach(item => {
                    item.dateStart = this.dateHelper.dateFormater(item.dtstart);
                    item.dateEnd = this.dateHelper.dateFormater(item.dtend);
                    this.eventList.push(item);
                });
            },
            (err) => {
                console.log(err);
            }
        )
    }
}