import { Component } from '@angular/core';
import { NavController, MenuController, NavParams, ToastController } from 'ionic-angular';
import { TagsListPage } from '../tagsList/tagsList';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { TranslationHelper } from '../../helpers/translation';

@Component({
    selector: 'page-tagsAdd',
    templateUrl: 'tagsAdd.html'
})
export class TagsAddPage {
    tag: {};
    tagsList: any;

    constructor(public navCtrl: NavController,
        public menuCtrl: MenuController,
        public translate: TranslateService,
        private storage: Storage,
        private navParams: NavParams,
        private toast: ToastController,
        private translationHelper: TranslationHelper) {
        this.menuCtrl.enable(true, 'mainMenu');
        this.tag = {
            "name": ""
        };
        this.tagsList = {
            tags: []
        };
        this.setTagsList();
    }

    /**
     * Method to set a tagsList
     */
    setTagsList() {
        const tagsList = this.navParams.get("tagsList")

        if (tagsList === undefined || tagsList === "") {
            this.tagsList = {
                tags: [JSON.parse(JSON.stringify(this.tag))]
            };
        } else {
            this.tagsList.tags = tagsList;
        }
    }

    /**
     * Method add new tag to list of tags
     */
    addNewTag() {
        this.tagsList.tags.push(JSON.parse(JSON.stringify(this.tag)));
    }

    /**
     * Method to remove the selected tag from the tags list
     * @param index number Index of tag on the tags list  
     */
    removeTag(index) {
        if (this.tagsList.tags.length !== 1) {
            this.tagsList.tags.splice(index, 1);
        }
    }

    /**
     * Method to save a tags list on storage
     */
    saveTagsList() {
        this.removeEmptyTags();
        if (this.tagsList.tags.length !== 0) {
            this.storage.set("tagsList", this.tagsList);
            this.toast.create({
                message: this.translationHelper.getTranslation("Tags", "Lista tagów zapisana prawidłowo"),
                duration: 2000,
                position: 'middle'
            }).present();
        } else {
            this.toast.create({
                message: this.translationHelper.getTranslation("Tags", "Lista tagów nie może mieć mniej tagów niż 1"),
                duration: 2000,
                position: 'middle'
            }).present();
        }
        this.navCtrl.setRoot(TagsListPage);
    }

    /**
     * Method to remove all empty tags
     */
    removeEmptyTags() {
        let numberOfRemoveElement = 0;
        this.tagsList.tags.forEach((element, index) => {
            if (element.name === "") {
                numberOfRemoveElement = this.tagsList.tags.splice(index, 1).length;
            }
        });

        if (numberOfRemoveElement === 0) {
            return;
        } else {
            this.removeEmptyTags();
        }
    }

}