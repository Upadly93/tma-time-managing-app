import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { TranslateService } from '@ngx-translate/core';
import { DatePicker } from '@ionic-native/date-picker';
import { TranslationHelper } from '../../helpers/translation';
import { HomePage } from '../home/home';
import { DateHelper } from '../../helpers/date';

@IonicPage()
@Component({
  selector: 'page-edit-event',
  templateUrl: 'editEvent.html',
})
export class EditEventPage {
  event = { title: "", eventLocation: "", notes: "", formatedDateStart: "", formatedDateEnd: "", dtstart: "", dtend: "" };
  oldEvent = { title: "", eventLocation: "", notes: "", formatedDateStart: "", formatedDateEnd: "", dtstart: "", dtend: "" };
  timestampStart: any;
  timestampEnd: any;

  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private menuCtrl: MenuController,
    private calendar: Calendar,
    public translate: TranslateService,
    public datePicker: DatePicker,
    private translationHelper: TranslationHelper,
    private dateHelper: DateHelper) {
    this.menuCtrl.enable(false, 'mainMenu');
    this.event = navParams.get("event");
    this.oldEvent = { ...this.event };
    this.timestampStart = this.event.dtstart;
    this.timestampEnd = this.event.dtend;
  }

  /**
   * Method run when user enter to this page
   */
  ionViewDidLoad() {
    console.log("ionViewDidLoad");
  }

  /**
   * Method to "update" a selected item - method delete current item and create a new item
   */
  updateEvent() {
    this.calendar.deleteEvent(this.oldEvent.title, this.oldEvent.eventLocation, this.oldEvent.notes, new Date(this.oldEvent.dtstart), new Date(this.oldEvent.dtstart)).then(
      (msg) => {
        this.addNewItem();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  /**
   * Method of adding the edited item
   */
  addNewItem() {
    this.calendar.createEvent(this.event.title, this.event.eventLocation, this.event.notes, new Date(this.timestampStart), new Date(this.timestampEnd)).then(
      (msg) => {
        let alert = this.alertCtrl.create({
          title: this.translationHelper.getTranslation("Toast", "Potwierdzenie"),
          subTitle: this.translationHelper.getTranslation("Toast", "Zdarzenie edytowano poprawnie"),
          buttons: ['OK']
        });
        alert.present();
        this.navCtrl.setRoot(HomePage);
      },
      (err) => {
        let alert = this.alertCtrl.create({
          title: this.translationHelper.getTranslation("Toast", "Błąd!"),
          subTitle: err,
          buttons: ['OK']
        });
        alert.present();
        console.log(err);
      }
    );

  }

  /**
   * Method to display native-datapicker to set a start data event
   */
  showDateStart() {
    this.datePicker.show({
      date: new Date(this.event.dtstart),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      is24Hour: true
    }).then(
      date => {
        this.event.formatedDateStart = this.dateHelper.dateFormater(date);
        this.timestampStart = new Date(date).getTime()
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  /**
   * Method to display native-datapicker to set a end data event
   */
  showDateEnd() {
    this.datePicker.show({
      date: new Date(this.event.dtend),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      is24Hour: true
    }).then(
      date => {
        this.event.formatedDateEnd = this.dateHelper.dateFormater(date);
        this.timestampEnd = new Date(date).getTime()
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }
}