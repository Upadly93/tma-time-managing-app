import { Component } from '@angular/core';
import { NavController, MenuController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { TranslationHelper } from '../../helpers/translation';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})

export class SettingsPage {
    pomodoro: {};
    time: number;
    loop: number;
    language: string;

    constructor(public navCtrl: NavController,
        public menuCtrl: MenuController,
        public translate: TranslateService,
        private storage: Storage,
        private toast: ToastController,
        private translationHelper: TranslationHelper) {
        this.menuCtrl.enable(true, 'mainMenu');
        this.getSettings();
    }

    /**
     * Method to getting all application settings
     */
    getSettings(){
        this.getPomodoroFromStorage();
        this.getAppLaguageFromStorage();
    }

    /**
     * Method to getting application language
     */
    getAppLaguageFromStorage() {
        this.storage.get("appLanguage").then(val => {
            if (val !== null) {
                this.language = val;
            } else {
                this.language = "en";
                this.setAppSettings(false);
            }
        });
    }

    /**
     * Method to getting pomodoro settings
     */
    getPomodoroFromStorage(){
        this.storage.get("pomodoro").then(val => {
            if (val !== null) {
                this.time = val.time;
                this.loop = val.loop;
            } else {
                this.time = 30;
                this.loop = 5;
                this.setPomodoro(false);
            }
        });
    }

    /**
     * Methot to set a pomodoro settings
     * @param showToast boolean whether display tost
     */
    setPomodoro(showToast) {
        this.pomodoro = {
            time: this.time,
            loop: this.loop
        };
        this.storage.set("pomodoro", this.pomodoro);
        if (showToast) {
            this.showToast();
        }
    }

    /**
     * Methot to set a application settings
     * @param showToast boolean whether display tost
     */
    setAppSettings(showToast) {
        this.storage.set("appLanguage", this.language);
        this.translate.use(this.language);
        if (showToast) {
            this.showToast();
        }
    }

    /**
     * Method to toggle cards in view settings
     * @param arrowId string selected arrow element
     * @param elementId string selected element
     */
    toggleCard(arrowId, elementId) {
        let element = document.getElementById(elementId);
        let arrow = document.getElementById(arrowId);

        if (element.style.display === "none") {
            element.style.display = "block";
            arrow.setAttribute("name", "arrow-up");
            arrow.setAttribute('ng-reflect-name', "arrow-up");
            arrow.setAttribute('aria-label', "arrow-up");
            arrow.setAttribute('class', "icon icon-ios ion-ios-arrow-up");
        } else {
            element.style.display = "none";
            arrow.setAttribute("name", "arrow-down");
            arrow.setAttribute('ng-reflect-name', "arrow-down");
            arrow.setAttribute('aria-label', "arrow-down");
            arrow.setAttribute('class', "icon icon-ios ion-ios-arrow-down");
        }
    }

    /**
     * Method to display toast
     */
    showToast() {
        this.toast.create({
            message: this.translationHelper.getTranslation("Settings", "Ustawienia zapisane prawidłowo"),
            duration: 1500,
            position: 'middle'
        }).present();
    }
}