import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TranslationHelper } from '../../helpers/translation';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

@Component({
    selector: 'page-registration',
    templateUrl: 'registration.html'
})
export class RegistrationPage implements OnInit {

    model = {
        "email": "",
        "password": "",
        "repeat_password": ""
    };

    registrationForm: FormGroup;

    validationMessages = {
        'email': [
            { type: 'required', message: 'Proszę podaj adres e-mail' },
            { type: 'pattern', message: 'Adres e-mail jest nieprawidłowy' },
        ],
        'password': [
            { type: 'required', message: 'Proszę podaj hasło' },
            { type: 'minlength', message: 'Hasło jest za krótkie (min 6 znaków)' },
            { type: 'maxlength', message: 'Hasło jest za długie (max 32 znaków)' },
            { type: 'pattern', message: 'Hasło musi zawierać małą i wielką literę oraz cyfrę' },
        ],
        'repeat_password': [
            { type: 'required', message: 'Proszę potwierdź hasło' },
        ],
    };

    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public afa: AngularFireAuth, public translate: TranslateService, private storage: Storage, private translationHelper: TranslationHelper) {
        this.menuCtrl.enable(false, 'mainMenu');
    }

    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';

    /**
     * Method for refreshing eye icon to hide and show password in form
     */
    hideShowPassword() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }

    /**
     * Method to register user
     */
    async register() {

        /* If form is not valid */
        if (this.registrationForm.valid === false) {
            return;
        }

        let errorContainer = document.getElementById("errorContainer");
        let errorTextContainer = document.getElementById("errorTextContainer");

        if (this.model.password !== this.model.repeat_password) {
            errorContainer.style.display = "block";
            errorTextContainer.innerText = "FormValidation.Hasła nie są identyczne";
            return;
        } else {
            // default not visible
            errorContainer.style.display = "none";
        }

        const registration = (navCtrl, translationHelper, afa, storage, model) => {
            this.afa.auth.createUserWithEmailAndPassword(model.email, model.password).then(function (user) {

                /* Zaloguj po poprawnej rejestracji */
                afa.auth.signInWithEmailAndPassword(model.email, model.password).then(function (user) {
                    storage.set('isLogged', true);
                    storage.set('email', model.email);
                    navCtrl.setRoot(HomePage);
                }, function (error) {

                    /**
                     * Function set error message in a form header
                     * @param message String
                     */
                    function setErrorMessage(message: String) {
                        errorContainer.style.display = "block";
                        errorTextContainer.innerText = translationHelper.getTranslation("AuthValidation", message);
                    }

                    switch (error.code) {
                        case 'auth/invalid-email':
                            setErrorMessage("Niepoprawny adres e-mail");
                            break;
                        case 'auth/user-disabled':
                            setErrorMessage("Użytkownik ma zablokowane konto");
                            break;
                        case 'auth/user-not-found':
                            setErrorMessage("Użytkownik nie został znaleziony");
                            break;
                        case 'auth/wrong-password':
                            setErrorMessage("Użytkownik nie został znaleziony"); //Wrong password
                            break;
                        default:
                            setErrorMessage(error.message);
                    }
                });

            }, function (error) {

                /**
                 * Function set error message in a form header
                 * @param message String
                 */
                function setErrorMessage(message: String) {
                    errorContainer.style.display = "block";
                    errorTextContainer.innerText = translationHelper.getTranslation("AuthValidation", message);
                }

                switch (error.code) {
                    case 'auth/invalid-email':
                        setErrorMessage("Niepoprawny adres e-mail");
                        break;
                    case 'auth/email-already-in-use':
                        setErrorMessage("Ten adres e-mail jest już zajęty");
                        break;
                    case 'auth/weak-password':
                        setErrorMessage("Hasło musi się składać z minimum 6 znaków");
                        break;
                    case 'auth/operation-not-allowed':
                        setErrorMessage("Rejestracja za pomocą adresu e-mail jest wyłączona");
                        break;
                    default:
                        setErrorMessage(error.message);
                }
            });
        }

        registration.call(this, this.navCtrl, this.translationHelper, this.afa, this.storage, this.model);

    }

    /**
     * Init registration form
     */
    ngOnInit() {
        let emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        let passwordPattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,32}$/;

        this.registrationForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.pattern(emailPattern)]),
            password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(32), Validators.pattern(passwordPattern)]),
            repeat_password: new FormControl('', [Validators.required]),
        });
    }

    /**
     * Method for back to login page button
     */
    backToLogin() {
        this.navCtrl.setRoot(LoginPage);
    }

}