import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

@Component({
    selector: 'page-timeMarker',
    templateUrl: 'timeMarker.html'
})
export class TimeMarkerPage {

    constructor(public navCtrl: NavController, public menuCtrl: MenuController) {
        this.menuCtrl.enable(true, 'mainMenu');
    }
}