import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { TagsAddPage } from '../tagsAdd/tagsAdd';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'page-tagsList',
    templateUrl: 'tagsList.html'
})
export class TagsListPage {
    tagsList: any;

    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public translate: TranslateService, private storage: Storage) {
        this.menuCtrl.enable(true, 'mainMenu');
        this.setTagsList();
    }

    /**
     * Method to set tags list
     */
    setTagsList() {
        this.storage.get("tagsList").then(val => {
            if (val === null || val === undefined) {
                this.tagsList = "";
            } else {
                this.tagsList = val.tags;
            }
        });
    }

    /**
     * Method to redirect to TagsAddPage
     */
    addNewTags() {
        this.navCtrl.setRoot(TagsAddPage, {
            tagsList: this.tagsList
        });
    }
}