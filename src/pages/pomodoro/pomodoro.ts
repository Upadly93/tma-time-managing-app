import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { PomodoroTimer } from '../../providers/PomodoroTimer';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'page-pomodoro',
    templateUrl: 'pomodoro.html'
})

export class PomodoroPage {

    constructor(public navCtrl: NavController, public menuCtrl: MenuController, private pomodoroTimer: PomodoroTimer, private storage: Storage) {
        this.menuCtrl.enable(true, 'mainMenu');

        if (this.pomodoroTimer.timer.hasStarted === false) {
            this.storage.get('pomodoro').then((pomodoro) => {
                this.pomodoroTimer.timer.timerLifeTime = pomodoro.time;
                this.pomodoroTimer.timer.displayTime = this.pomodoroTimer.timer.timerLifeTime + ":" + "00";
            });
        }
    }
}