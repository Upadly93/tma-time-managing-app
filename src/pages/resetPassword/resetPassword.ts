import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
    selector: 'page-reset-pass',
    templateUrl: 'resetPassword.html'
})
export class ResetPasswordPage implements OnInit {

    model = {
        "email": ""
    };

    resetPassForm: FormGroup;
    validationMessages: Object;

    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public translate: TranslateService, private toastCtrl: ToastController, public afa: AngularFireAuth, public alertCtrl: AlertController) {

        this.validationMessages = {
            'email': [
                { type: 'required', message: 'Proszę podaj adres e-mail' },
                { type: 'pattern', message: 'Adres e-mail jest nieprawidłowy' },
            ]
        }
    }

    async resetPassword() {
        /* If form is not valid */
        if (this.resetPassForm.valid === false) {
            return;
        }

        const resetPassword = (navCtrl, alertCtrl, translate, toast, model) => {
            this.afa.auth.sendPasswordResetEmail(model.email).then(function (user) {

                let messages = {
                    desc: "",
                    close: ""
                };

                translate.get("Toast.Email do zresetowania hasła został wysłany. Sprawdź swoją skrzynkę.").subscribe((res: string) => {
                    messages.desc = res;
                });
                translate.get("Toast.Zamknij").subscribe((res: string) => {
                    messages.close = res;
                });

                toast.create({
                    'message': messages.desc,
                    duration: 5000,
                    position: 'middle',
                    showCloseButton: true,
                    closeButtonText: messages.close,
                }).present();

                navCtrl.setRoot(LoginPage);
            }, function (error) {

                function setErrorMessage(message: String) {
                    translate.get("AuthValidation." + message).subscribe((res: string) => {
                        let alert = alertCtrl.create({
                            subTitle: res,
                            buttons: ['OK']
                        });
                        alert.present();
                    });
                }

                switch (error.code) {
                    case 'auth/invalid-email':
                        setErrorMessage("Niepoprawny adres e-mail");
                        break;
                    case 'auth/user-not-found':
                        setErrorMessage("Użytkownik nie został znaleziony");
                        break;
                    default:
                        setErrorMessage(error.message);
                }
            });

        }

        resetPassword.call(this, this.navCtrl, this.alertCtrl, this.translate, this.toastCtrl, this.model);
    }

    ngOnInit() {
        let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        this.resetPassForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)])
        });
    }

    goBack() {
        this.navCtrl.setRoot(LoginPage);
    }
}