import { Injectable } from '@angular/core';

/**
 * Helper class for date
 */
@Injectable()
export class DateHelper {
    constructor() { }


    /**
     * Method to format data to display from timestamp
     * @param timestamp number Timestamp
     */
    dateFormater(timestamp) {
        const date = new Date(timestamp);

        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        const hours = date.getHours();
        const minutes = date.getMinutes();
        let formatedMinutes: any;
        let formatedHours: any;

        formatedHours = hours < 10 ? "0" + hours : hours;
        formatedMinutes = minutes < 10 ? "0" + minutes : minutes;

        const dateFormated = day + "." + month + "." + year + " " + formatedHours + ":" + formatedMinutes;

        return dateFormated;
    }
}
