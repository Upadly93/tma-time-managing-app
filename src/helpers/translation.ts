import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

/**
 * Helper class for translations
 */
@Injectable()
export class TranslationHelper {
    constructor(public translate: TranslateService) { }

    /**
    * Method to get translation
    * @param category string Category of translation
    * @param name string Name string to translation
    */
    getTranslation(category, name) {
        let translation;

        this.translate.get(category + "." + name).subscribe((res: string) => {
            translation = res;
        })

        return translation;
    }
}
