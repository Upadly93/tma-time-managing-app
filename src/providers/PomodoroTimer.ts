import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { TranslationHelper } from '../helpers/translation';

@Injectable()
export class PomodoroTimer {

    timeout: number;
    eventsList: Array<any>;

    timer = {
        "hasStarted": false,
        "isPaused": false,
        "startTime": 0,
        "endTime": 0,
        "pausedTime": 0,
        "timerLifeTime": 0,
        "displayTime": "",
        "timerLoop": 0
    };

    constructor(private storage: Storage, private alertCtrl: AlertController, private translationHelper: TranslationHelper) {
        this.storage.get('pomodoroEvents').then((pomodoroEvents) => {
            this.eventsList = pomodoroEvents;

            /* Refresh events list */
            this.refreshList();
        });
    }

    /**
     * Method for play button
     */
    playTimerFromButton() {
        this.storage.get('pomodoro').then((pomodoro) => {
            this.timer.timerLoop = pomodoro.loop;
        });

        this.startTimer();
    }

    /**
     * Method start a timer and set timer object
     */
    startTimer() {
        let date = new Date();

        if (this.timer.hasStarted === false) { /* if it is a new timer */
            this.timer.hasStarted = true;
            this.timer.startTime = date.getTime();
            this.timer.endTime = date.getTime() + (this.timer.timerLifeTime * 60 * 1000);
        } else if (this.timer.isPaused === true) { /* if unpaused action */
            let time = this.timer.displayTime;

            if (time !== "") {
                /* Time left */
                let mins = parseInt(time.substring(0, 2));
                let seconds = parseInt(time.substring(3));

                this.timer.isPaused = false;
                this.timer.endTime = date.getTime() + ((mins * 60 + seconds) * 1000);
                this.timer.startTime = this.timer.endTime - (this.timer.timerLifeTime * 1000 * 60);
                this.timer.pausedTime = 0;
            } else {
                this.clearTimerObj();
                return;
            }
        }

        this.timerTick();
    }

    /**
     * The method representing the ticking of the timer
     */
    timerTick() {
        if (this.timer.isPaused === false) {
            this.timeout = setTimeout(() => {
                let timeDiff = Math.round((this.timer.endTime - new Date().getTime()) / 1000);

                let mins = Math.floor(timeDiff / 60);
                let seconds = timeDiff - (mins * 60);

                if (mins >= 0 && seconds >= 0) {
                    this.timer.displayTime = (mins < 10 ? "0" : "") + mins + ":" + (seconds < 10 ? "0" : "") + seconds;
                    this.timerTick();
                } else {
                    this.refreshTimerObjToRepeat();
                    this.displayPopup();
                }

            }, 1000);
        }
    }

    /**
     * Method to pause timer
     */
    pauseTimer() {
        if (this.timer.hasStarted === true) {
            clearTimeout(this.timeout);
            this.timer.isPaused = true;

            let date = new Date();
            this.timer.pausedTime = date.getTime();
        }
    }

    /**
     * Method to stop timer and clear timer object
     */
    stopTimer() {
        if (this.timer.hasStarted === true) {
            clearTimeout(this.timeout);
            this.clearTimerObj();
        }
    }

    /**
     * Method for clearing timer object
     */
    clearTimerObj() {
        this.timer.hasStarted = false;
        this.timer.isPaused = false;
        this.timer.startTime = 0;
        this.timer.endTime = 0;
        this.timer.pausedTime = 0;

        this.storage.get('pomodoro').then((pomodoro) => {
            this.timer.timerLoop = pomodoro.loop;
            this.timer.timerLifeTime = pomodoro.time;
            this.timer.displayTime = this.timer.timerLifeTime + ":" + "00";
        });
    }

    /**
     * Method refreshing timer object for pomodoro timer repeating functionality
     */
    refreshTimerObjToRepeat() {
        this.timer.hasStarted = false;
        this.timer.isPaused = false;
        this.timer.startTime = 0;
        this.timer.endTime = 0;
        this.timer.pausedTime = 0;
        this.timer.displayTime = this.timer.timerLifeTime + ":" + "00";
    }

    /**
     * Method displaying a popup with 'What do you do now?' question
     */
    displayPopup() {

        let alert = this.alertCtrl.create({
            title: this.translationHelper.getTranslation("MenuBar", "Pomodoro"),
            subTitle: this.translationHelper.getTranslation("Pomodoro", "Co aktualnie robisz?"),

            inputs: [
                {
                    name: 'event'
                }
            ],
            buttons: [
                {
                    text: this.translationHelper.getTranslation("Common", "Zapisz"),
                    role: 'save',
                    handler: data => {

                        this.storage.get("pomodoroEvents").then((pomodoroEvents) => {
                            /* Initialize if empty */
                            if (pomodoroEvents === null) {
                                this.storage.set("pomodoroEvents", []);
                                pomodoroEvents = [];
                            }

                            this.eventsList = pomodoroEvents;

                            this.eventsList.unshift({
                                eventName: data.event,
                                timestamp: new Date().getTime()
                            });

                            this.storage.set("pomodoroEvents", this.eventsList);
                            this.refreshList();
                            this.repeatTimer();
                        });
                    }
                },
                {
                    text: this.translationHelper.getTranslation("Common", "Anuluj"),
                    role: 'cancel'
                }
            ]
        });

        alert.present();
    }

    /**
     * Method for refreshing a pomodoro events list
     */
    refreshList() {
        if (this.eventsList !== undefined && this.eventsList !== null) {
            let oneDay = 24 * 60 * 60 * 1000; // in milliseconds
            let last24HoursTimestamp = new Date().getTime() - oneDay;

            this.eventsList = this.eventsList.filter(event => last24HoursTimestamp - event.timestamp <= oneDay);
            this.storage.set("pomodoroEvents", this.eventsList);
        }
    }

    /**
     * Method for a pomodoro timer repeating functionality
     */
    repeatTimer() {
        this.timer.timerLoop--;

        if (this.timer.timerLoop > 0) {
            this.startTimer();
        } else {
            this.clearTimerObj();
        }
    }
}